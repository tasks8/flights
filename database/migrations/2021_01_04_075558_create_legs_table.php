<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLegsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legs', function (Blueprint $table) {
            $table->id();

            $table->string('code');
            $table->char('departure_airport', 3);
            $table->char('arrival_airport', 3);

            $table->dateTime('departure_time');
            $table->dateTime('arrival_time');

            $table->tinyInteger('stops')->unsigned()->default(0);
            $table->smallInteger('duration_mins')->unsigned();
            $table->foreignId('airline_id')->constrained('airlines')->onDelete('cascade');

            $table->timestamps();

            $table->unique('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legs');
    }
}
