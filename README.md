## Requirements
  - Docker Engine 20+
  - Docker Compose 1.20+ (Tested on 1.27.4)
## Installation
  1. Install composer dependencies: 
    ```docker run --rm -v $(pwd):/opt -w /opt laravelsail/php80-composer:latest composer install```
  2. Make sure to create your `.env` file (sail uses your DB configuration from .env as base to generate credentials)   
  2. Start up docker instance: ```./vendor/bin/sail up```
  3. Launch migrations: ```./vendor/bin/sail artisan migrate```
  4. Import flights data: ```./vendor/bin/sail artisan import:flights flights.json``` (Data is already placed here storage/import/flights.json)   
  5. Install NPM packages ```./vendor/bin/sail npm install```
  6. Build Assets ```./vendor/bin/sail npm run prod```
  7. Open the site in browser

## Answers
 - What libraries did you add to the frontend? What are they used for?
    - Vue 2.6 - as a templating engine, nothing too special
    - TailwindCSS - For faster CSS prototyping 
 - If you had more time, what further improvements or new features would you add?
    - CSS Framework with more boilerplate classes like bootstrap 
    - Add tests to reservation form 
 - Which parts are you most proud of? And why?
    - Nothing in particular, all of the code is pretty simple and straightforward
 - Which parts did you spend the most time with? What did you find most difficult?
    - The bigger chunk of the development time was spent mostly writing the frontend styles with TailwindCSS
    - There were not any difficult parts but there were some issues that popped up during development with webpack - ES6 modules + URL loader
 - How did you find the test overall? Did you have any issues or have difficulties completing?
    - Simple and straightforward
