<?php

use App\Http\Controllers\Api\ItinerariesController;
use App\Http\Controllers\Api\ReservationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('itineraries', [ItinerariesController::class, 'index']);
Route::post('itineraries/{itinerary}/reserve', [ReservationController::class, 'submit'])->middleware(['throttle:5,1']);
