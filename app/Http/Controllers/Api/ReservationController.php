<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\ReservationRequest;
use App\Models\Itinerary;

class ReservationController extends Controller
{
    /**
     * @param  ReservationRequest  $request
     * @param  Itinerary  $itinerary
     * @return array
     */
    public function submit(ReservationRequest $request, Itinerary $itinerary): array
    {
        $itinerary->reservations()->create($request->all());

        return [
            'status' => true
        ];
    }
}
