<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Resources\ItineraryResource;
use App\Models\Itinerary;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ItinerariesController extends Controller
{
    /**
     * @return ResourceCollection
     */
    public function index(): ResourceCollection
    {
        return ItineraryResource::collection(
            Itinerary::query()
                ->inRandomOrder()
                ->get()
        );
    }
}
