<?php

namespace App\Http\Resources;

use App\Models\Leg;
use Carbon\CarbonInterval;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Leg */
class LegResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $interval = CarbonInterval::minutes($this->duration_mins);

        return [
            'airline' => AirlineResource::make($this->airline),
            'departure_airport' => $this->departure_airport,
            'arrival_airport' => $this->arrival_airport,
            'departure_time' => $this->departure_time->format('H:i'),
            'arrival_time' => $this->arrival_time->format('H:i'),
            'stops' => $this->stops,
            'duration' => $interval->cascade()->forHumans(short:true, parts:2)
        ];
    }
}
