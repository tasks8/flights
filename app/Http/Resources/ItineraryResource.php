<?php

namespace App\Http\Resources;

use App\Models\Itinerary;
use App\Services\CurrencyFormatter;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Itinerary */
class ItineraryResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getKey(),
            'legs' => LegResource::collection($this->legs),
            'price' => app(CurrencyFormatter::class)->format($this->currency_code, $this->price),
            'agent' => AgentResource::make($this->agent),
        ];
    }
}
