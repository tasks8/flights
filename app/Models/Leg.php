<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Leg extends Model
{
    use HasFactory;

    protected $dates = [
        'arrival_time',
        'departure_time',
    ];

    protected $fillable = [
        'code',
        'departure_airport',
        'arrival_airport',
        'arrival_time',
        'departure_time',
        'stops',
        'duration_mins',
    ];

    /**
     * @return BelongsTo
     */
    public function airline(): BelongsTo
    {
        return $this->belongsTo(Airline::class);
    }
}
