<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Agent extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'rating',
    ];

    /**
     * @return HasMany
     */
    public function itineraries(): HasMany
    {
        return $this->hasMany(Itinerary::class);
    }
}
