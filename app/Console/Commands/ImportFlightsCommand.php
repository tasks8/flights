<?php

namespace App\Console\Commands;

use App\Models\Agent;
use App\Models\Airline;
use App\Models\Itinerary;
use App\Models\Leg;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\Factory as FilesystemFactory;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ImportFlightsCommand extends Command
{
    protected const DEFAULT_CURRENCY = 'EUR';

    protected array $importedLegs = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:flights {file} {--disk=import}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initial flight data import';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(FilesystemFactory $filesystemFactory)
    {
        $file = $this->argument('file');
        $disk = $this->option('disk');

        $filesystem = $filesystemFactory->disk($disk);

        if (!$filesystem->exists($file)) {
            throw new \LogicException("File '{$file}' not found in disk '{$disk}'");
        }

        $data = json_decode($filesystem->get($file), 1);

        $this->importLegs($data['legs']);
        $this->importItineraries($data['itineraries']);
    }

    /**
     * @param  array  $legs
     */
    protected function importLegs(array $legs)
    {
        $existingCodes = Leg::query()
            ->whereIn('code', Arr::pluck($legs, 'id'))
            ->pluck('code', 'id');

        foreach ($legs as $legData) {
            $legCode = $legData['id'];
            $legId = $existingCodes->search($legCode, true);

            if ($legId === false) {
                $airline = Airline::query()->firstOrCreate(['code' => $legData['airline_id']], [
                    'title' => $legData['airline_name'],
                ]);

                $leg = new Leg($legData);
                $leg->code = $legCode;
                $leg->airline()->associate($airline);
                $leg->save();

                $legId = $leg->getKey();
            }

            $this->importedLegs[$legCode] = $legId;
        }
    }

    /**
     * @param  array  $itineraries
     *
     * @return void
     */
    protected function importItineraries(array $itineraries)
    {
        $existingCodes = Leg::query()
            ->whereIn('code', Arr::pluck($itineraries, 'id'))
            ->pluck('code', 'id');

        foreach ($itineraries as $itineraryData) {
            $code = $itineraryData['id'];

            if ($existingCodes->search($code, true) !== false) {
                continue;
            }

            $agent = Agent::query()->firstOrCreate(['title' => $itineraryData['agent']], [
                'rating' => $itineraryData['agent_rating']
            ]);

            $itinerary = new Itinerary($itineraryData);
            $itinerary->code = $code;
            $itinerary->agent()->associate($agent);

            $itinerary->price = preg_replace('/[^-.0-9]/', '', $itineraryData['price']);
            $itinerary->currency_code = $this->parseCurrencyCode($itineraryData['price']) ?: static::DEFAULT_CURRENCY;

            $itinerary->save();

            $itinerary->legs()->sync(array_map(fn($legCode) => $this->importedLegs[$legCode], $itineraryData['legs']));
        }
    }

    /**
     * @param  string  $priceText
     * @return string|null
     */
    protected function parseCurrencyCode(string $priceText): ?string
    {
        if (Str::startsWith($priceText, '£')) {
            return 'GBP';
        }

        return null;
    }
}
