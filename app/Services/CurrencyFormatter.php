<?php


namespace App\Services;


class CurrencyFormatter
{
    private const FORMATS = [
        'GBP' => ['£', '{symbol}{number}'],
    ];

    /**
     * @param  string  $currencyCode
     * @param $amount
     * @return string
     */
    public function format(string $currencyCode, $amount): string
    {
        $currencyCode = strtoupper($currencyCode);

        if( ! array_key_exists($currencyCode, static::FORMATS)) {
            throw new \LogicException("Currency '{$currencyCode}' has no defined format.");
        }

        [$symbol, $format] = static::FORMATS[$currencyCode];

        return str_replace(['{symbol}', '{number}'], [$symbol, number_format($amount)], $format);
    }
}
