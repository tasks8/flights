@extends('layouts.app')

@section('content')
    <registration-form></registration-form>

    <itineraries></itineraries>
@endsection
