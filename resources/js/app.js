require('./bootstrap');

Vue.component('itineraries', require('./components/Itineraries.vue').default)
Vue.component('registration-form', require('./components/ReservationForm.vue').default)

//
const app = document.getElementById('app');
(new Vue).$mount(app);
